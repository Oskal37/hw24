﻿#include <iostream>

void evenOrOddNumber(bool iseven, int N)
{
    if (iseven)
    {
        for (int i = 0; i <= N; i++)
        {
            if (i % 2 == 0)
            {
                std::cout << i;
            }
        }
    }
    else
    {
        for (int i = 0; i <= N; i++)
        {
            if (i % 2 != 0)
            {
                std::cout << i;
            }
        }
    }
    std::cout << "\n";
}

int main()
{   int n;
    std::cout << "Enter N\n";
    std::cin >> n;    
    for (int i = 0; i <= n; i++) 
    {
        if (i % 2 == 0) 
        {
            std::cout << i;
        }
    }
    std::cout << "\n";
    std::cout << "Even number N\n";
    evenOrOddNumber(true, n);
    std::cout << "Odd number N\n";
    evenOrOddNumber(false, n);
}


